# Sublime Text 3 

List of plugins, themes, and tools we use for the day by day development.

1. Plugins
All plugins are installed via Package Control
  * Emmet
  * GitGutter
  * SideBar
  * EmmetLivestyle
  * TypeScript
  * SASS / SCSS
  * LESS
  * Alignment
  * BracketHightlighted
2. Theme
  We're useing [Material Theme](https://github.com/equinusocio/material-theme) + [Material Theme Appbar](https://github.com/equinusocio/material-theme-appbar)
3. Tools
  * Git (github, gitlab)
  * Grunt Task Manager


